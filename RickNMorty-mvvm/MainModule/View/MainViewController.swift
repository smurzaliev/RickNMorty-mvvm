//
//  ViewController.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 30.06.2022.
//

import UIKit
import SnapKit
import SwiftUI

class MainViewController: UIViewController {
        
    private var viewModel: MainViewModel?
    
    private lazy var charTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubview()
        setupNavTitle()
        bindViewModel()
        viewModel?.getChars()
    }

    private func setupSubview() {
        view.backgroundColor = .white
        view.addSubview(charTable)
        charTable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func bindViewModel() {
        viewModel?.chars.bind({ [weak self] _ in
            DispatchQueue.main.async {
                self?.charTable.reloadData()
            }
        })
    }
    
    private func setupNavTitle() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = "Characters"
        self.navigationItem.accessibilityLabel = "Characters"
    }

}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.chars.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = CharCell()
        let model = viewModel?.chars.value?[index]
        cell.fill(char: model!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

