//
//  CharCell.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 30.06.2022.
//

import UIKit
import SnapKit
import Kingfisher

class CharCell: UITableViewCell {
    
    private lazy var charImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var charId: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10, weight: .semibold)
        view.textColor = .orange
        return view
    }()
    
    private lazy var charTitle: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    private lazy var charSpecies: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        return view
    }()
    
    private lazy var charOrigin: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        return view
    }()
    
    private lazy var charStatus: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        return view
    }()

    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        backgroundColor = .white
        
        addSubview(charImage)
        charImage.snp.makeConstraints { make in
            make.height.equalTo(self.frame.height - 10)
            make.width.equalTo(self.frame.height - 10)
            make.left.equalToSuperview().offset(5)
            make.top.equalToSuperview().offset(5)
        }
        
        addSubview(charTitle)
        charTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.left.equalTo(charImage.snp.right).offset(8)
        }
        
        addSubview(charId)
        charId.snp.makeConstraints { make in
            make.top.equalTo(charTitle.snp.bottom)
            make.left.equalTo(charImage.snp.right).offset(8)
        }
        
        addSubview(charSpecies)
        charSpecies.snp.makeConstraints { make in
            make.top.equalTo(charId.snp.bottom)
            make.left.equalTo(charImage.snp.right).offset(8)
        }
        
        addSubview(charStatus)
        charStatus.snp.makeConstraints { make in
            make.top.equalTo(charSpecies.snp.bottom)
            make.left.equalTo(charImage.snp.right).offset(8)
        }
        
        addSubview(charOrigin)
        charOrigin.snp.makeConstraints { make in
            make.top.equalTo(charStatus.snp.bottom)
            make.left.equalTo(charImage.snp.right).offset(8)
        }
    }
    
    func fill(char: Result) {
        charTitle.text = "Name: \(char.name)"
        charId.text = "ID: \(char.id)"
        charSpecies.text = "Species: \(char.species.rawValue)"
        charStatus.text = "Status: \(char.status.rawValue)"
        charOrigin.text = "Origin: \(char.origin.name)"
        charImage.kf.setImage(with: URL(string: char.image))
    }

}
