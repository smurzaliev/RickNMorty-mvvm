//
//  MainViewModel.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 30.06.2022.
//

import Foundation

class MainViewModel {
    
    let networkApi: NetworkService!
    
    var chars: Observable<[Result]> = Observable([])
    
    required init(_ networkApi: NetworkService) {
        self.networkApi = networkApi
    }
    
    func getChars() {
        
        networkApi.getAllChars(completion: { [weak self] charList in
            self?.chars.value = []
            for item in charList {
                self?.chars.value?.append(item)
            }
        })
    }
}
