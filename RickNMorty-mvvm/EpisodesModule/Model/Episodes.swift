//
//  Episodes.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 01.07.2022.
//

import Foundation

// MARK: - Episodes
struct Episodes: Codable {
    let info: Info
    let results: [EpisodeResults]
}

// MARK: - Info
struct Infos: Codable {
    let count, pages: Int
    let next: String
    let prev: JSONNull?
}

// MARK: - Result
struct EpisodeResults: Codable {
    let id: Int
    let name, airDate, episode: String
    let characters: [String]
    let url: String
    let created: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case airDate = "air_date"
        case episode, characters, url, created
    }
}

// MARK: - Encode/decode helpers

class JSONNull1: Codable, Hashable {

    public static func == (lhs: JSONNull1, rhs: JSONNull1) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

