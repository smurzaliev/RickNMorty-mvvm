//
//  EpisodesCell.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 01.07.2022.
//

import UIKit
import SnapKit

class EpisodesCell: UITableViewCell {
    
    private lazy var episodeId: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10, weight: .semibold)
        view.textColor = .orange
        return view
    }()
    
    private lazy var episodeName: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    private lazy var episodeDate: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        return view
    }()
    
    private lazy var episodeS: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        return view
    }()

    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        backgroundColor = .white
        
        addSubview(episodeS)
        episodeS.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(5)
        }
        
        addSubview(episodeId)
        episodeId.snp.makeConstraints { make in
            make.top.equalTo(episodeS.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(5)
        }
        
        addSubview(episodeName)
        episodeName.snp.makeConstraints { make in
            make.top.equalTo(episodeId.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(5)
        }
        
        addSubview(episodeDate)
        episodeDate.snp.makeConstraints { make in
            make.top.equalTo(episodeName.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(5)
        }
    }
    
    func fill(episode: EpisodeResults) {
        episodeS.text = "Episode: \(episode.episode)"
        episodeId.text = "ID: \(episode.id)"
        episodeName.text = "Title: \(episode.name)"
        episodeDate.text = "Date: \(episode.airDate)"
    }
}
