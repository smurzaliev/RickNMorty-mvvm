//
//  RandomViewModel.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 01.07.2022.
//

import Foundation

class EpisodesViewModel {
    
    let networkApi: NetworkService!
    
    var episodes: Observable<[EpisodeResults]> = Observable([])
    
    required init(_ networkApi: NetworkService) {
        self.networkApi = networkApi
    }
    
    func getAllEpisodes() {
        networkApi.getAllEpisodes { newEpisodes in
            self.episodes.value = []
            for episode in newEpisodes {
                self.episodes.value?.append(episode)
            }
        }
    }
}
