//
//  RandomViewController.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 01.07.2022.
//

import UIKit
import SnapKit
import Kingfisher

class EpisodesViewController: UIViewController {
    
    private var viewModel: EpisodesViewModel?
    
    private lazy var episodesTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    init(viewModel: EpisodesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubview()
        setupNavTitle()
        bindViewModel()
        viewModel?.getAllEpisodes()
    }

    private func setupSubview() {
        view.backgroundColor = .white
        view.addSubview(episodesTable)
        episodesTable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func bindViewModel() {
        viewModel?.episodes.bind({ [weak self] _ in
            DispatchQueue.main.async {
                self?.episodesTable.reloadData()
            }
        })
    }
    
    private func setupNavTitle() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = "Episodes"
        self.navigationItem.accessibilityLabel = "Episodes"
    }

}

extension EpisodesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.episodes.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let cell = EpisodesCell()
        let model = viewModel?.episodes.value?[index]
        cell.fill(episode: model!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

