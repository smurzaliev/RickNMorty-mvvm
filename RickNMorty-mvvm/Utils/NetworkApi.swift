//
//  NetworkApi.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 30.06.2022.
//

import Foundation
import Alamofire

protocol NetworkService {
    func getAllChars(completion: @escaping ([Result]) -> ())
    func getAllEpisodes(completion: @escaping ([EpisodeResults]) -> ())
}

class NetworkApi: NetworkService {
    
    static let sharedInstance = NetworkApi()

    let baseURL = "https://rickandmortyapi.com/api"
    
    func getAllChars(completion: @escaping ([Result]) -> ()) {
        let url = "\(baseURL)/character"
        let request = AF.request(url, method: .get)
        request.responseDecodable { (response : DataResponse<Characters,AFError>) in
            switch response.result {
            case.failure(let error):
                print("Error \(error.localizedDescription)")
                completion([])
            case .success(let data):
                completion(data.results)
            }
        }
    }
    
    func getAllEpisodes(completion: @escaping ([EpisodeResults]) -> ()) {
        let url = "\(baseURL)/episode"
        let request = AF.request(url, method: .get)
        request.responseDecodable { (response : DataResponse<Episodes,AFError>) in
            switch response.result {
            case.failure(let error):
                print("Error \(error.localizedDescription)")
                completion([])
            case .success(let data):
                completion(data.results)
            }
        }
    }
}
