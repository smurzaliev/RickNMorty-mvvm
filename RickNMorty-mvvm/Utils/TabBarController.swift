//
//  TabBarController.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 30.06.2022.
//

import Foundation
import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        configureTabBarItems()
    }

    private func configureTabBarItems() {
        let mainVM = MainViewModel(NetworkApi.sharedInstance)
        let mainVC = MainViewController(viewModel: mainVM)
        mainVC.tabBarItem = UITabBarItem(title: "Characters", image: UIImage(systemName: "1.circle"), tag: 0)
        
        let episodeVM = EpisodesViewModel(NetworkApi.sharedInstance)
        let episodeVC = EpisodesViewController(viewModel: episodeVM)
        episodeVC.tabBarItem = UITabBarItem(title: "Episodes", image: UIImage(systemName: "2.circle"), tag: 0)
      
        let mainNavVC = UINavigationController(rootViewController: mainVC)
        let episodeNavVC = UINavigationController(rootViewController: episodeVC)

        setViewControllers([mainNavVC, episodeNavVC], animated: true)
    }
}
