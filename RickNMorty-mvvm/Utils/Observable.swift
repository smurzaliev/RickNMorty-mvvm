//
//  Observable.swift
//  RickNMorty-mvvm
//
//  Created by Samat Murzaliev on 30.06.2022.
//

import Foundation

class Observable<T> {

    typealias Listener = (T?) -> Void
    private var listeners: [Listener] = []
    var value: T? {
        didSet {
            listeners.forEach {
                $0(value)
            }
        }
    }
    
    init(_ v:T?) {
        value = v
    }
    
    func bind(_ listener: @escaping Listener) {
        listener(value)
        self.listeners.append(listener)
    }
}
